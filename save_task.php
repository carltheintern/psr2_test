<?php
	$tid = $_POST["id"];
	$tname = $_POST["name"];

	try {
		$myPDO = new PDO('mysql:host=localhost;dbname=psr2_test', 'root', '');
		$stmt = $myPDO->prepare("INSERT INTO psr2_test (id, name) VALUES (tid,tname)");
	    $stmt->bindParam(':tid', $tid);
	    $stmt->bindParam(':tname', $tname);
	    $stmt->execute();
	} catch (PDOException $exception) {
		return $exception->getMessage();
	}
?>